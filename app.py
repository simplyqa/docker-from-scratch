from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    "https://media.giphy.com/media/tBb19eVXSNsaa6k8tjO/giphy.gif",
    "https://media.giphy.com/media/wmEFhlivchtxS/giphy.gif",
    "https://media.giphy.com/media/3o7bu1P8ITfnZzBi0g/giphy.gif",
    "https://media.giphy.com/media/LgwoVr7YgUkrC/giphy.gif",
    "https://media.giphy.com/media/4Z3DdOZRTcXPa/giphy.gif",
    "https://media.giphy.com/media/cbb8zL5wbNnfq/giphy.gif",
    "https://media.giphy.com/media/4GRRBtKrdiFDa/giphy.gif",
    "https://media.giphy.com/media/Ph2EUpM4KAmAM/giphy.gif",
    "https://media.giphy.com/media/10spcFioEOY7zG/giphy.gif"
]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")

